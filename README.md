#### Install Packages

```sh
make install
```

#### Complie

```sh
make compile
```

#### Sync

```sh
make sync
```
