#
# This file is autogenerated by pip-compile with Python 3.9
# by the following command:
#
#    pip-compile requirements-dev.in
#
attrs==22.1.0
    # via pytest
build==0.9.0
    # via pip-tools
click==8.1.3
    # via pip-tools
colorama==0.4.6
    # via
    #   build
    #   click
    #   pytest
exceptiongroup==1.0.4
    # via pytest
iniconfig==1.1.1
    # via pytest
packaging==22.0
    # via
    #   build
    #   pytest
pep517==0.13.0
    # via build
pip-tools==6.12.1
    # via -r requirements-dev.in
pluggy==1.0.0
    # via pytest
pytest==7.2.0
    # via
    #   -r requirements-dev.in
    #   pytest-django
pytest-django==4.5.2
    # via -r requirements-dev.in
tomli==2.0.1
    # via
    #   build
    #   pep517
    #   pytest
wheel==0.38.4
    # via pip-tools

# The following packages are considered to be unsafe in a requirements file:
# pip
# setuptools
